### Problem
In the following situation, babel can generate invalid code from a valid input:
 - An async function is decorated
 - Decorators are transformed using `@babel/plugin-proposal-decorators` in `legacy` (stage 1) mode.
 - `@babel/preset-env` is used targeting a version of Node that does not require `regenerator-runtime` (i.e. 12)
 - `retainLines` is set to `true`

`retainLines` causes babel to split the `async functionName()` into two lines:
```javascript
async
functionName()
```
This is interpreted as a random identifier expression `async`, followed by the declaration of a non-async function. 
This means that any `await` usage inside the function is treated as invalid. The issue is not present when babel uses
transforms that involve `regenerator-runtime`, since the async/await keywords are stripped from the code in that
case. This is also not an issue when `async` functions don't have decorators.

### Reproduction
This repo is set up with a minimal babel configuration and example of this issue. Use `yarn run babel` to view
the generated code, and notice the error on lines 6/7 (which must be combined into a single line to be correct). To
view the error, either `node dist/index.js` or `yarn run start` (which uses `babel-node` to skip the build step).

### History
 - A related report (from Sep. 2018) can be found in [#8650](https://github.com/babel/babel/issues/8650)
 - [#8650](https://github.com/babel/babel/issues/8650) was closed in favor of a related issue: [#7275](https://github.com/babel/babel/issues/7275)
 - [#7275](https://github.com/babel/babel/issues/7275) was marked closed as a result of PR [#8868](https://github.com/babel/babel/pull/8868) in Oct 2018
 - A very similar (but not quite the same) issue just surfaced about a week ago: [#11830](https://github.com/babel/babel/issues/11830)
 - [#11830](https://github.com/babel/babel/issues/11830) was fixed by a recent PR: [#11836](https://github.com/babel/babel/pull/11836)
 - This PR hasn't yet landed in a release. *However*, I can still reproduce this issue on main (by cloning the babel
 repo and `yarn link`ing the latest contents of main, including this PR, into this test repo.)

