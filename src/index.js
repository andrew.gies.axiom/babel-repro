const TestDecorator = (target, _property, descriptor) => {
  console.log("Decorated!");
};

class Test {
  @TestDecorator
  async decorateMe() {
    const text = await Promise.resolve('pinky swear');
    console.log(text);
  }
}
